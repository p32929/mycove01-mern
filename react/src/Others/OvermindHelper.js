// npm install overmind overmind-react
// yarn add overmind overmind-react

import {createOvermind} from "overmind";
import {createHook} from "overmind-react";

export const useOvermind = createHook();
export const overmind = createOvermind({
    state: {
        status: ""
    },
    actions: {
        setStatus({state}, status) {
            state.status = status
        }
    }
});
