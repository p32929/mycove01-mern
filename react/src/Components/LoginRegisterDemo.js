import React from 'react';
import Grid from "@material-ui/core/Grid";
import {makeStyles} from '@material-ui/core/styles';
import {useOvermind} from "../Others/OvermindHelper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {useFormik} from 'formik';
import {FayFetch} from 'fayfetch'

const apiEndpoint = "http://localhost:2000/users/"
const loginEndpoint = apiEndpoint + "login/"
const registerEndpoint = apiEndpoint + "register/"

const getThemeObj = (theme) => {
    return {
        commonStyles: {
            marginTop: 8
        },
        topText: {
            marginTop: 32
        }
    }
}

const useStyles = makeStyles((theme) => (getThemeObj(theme)))

const LoginRegisterDemo = (props) => {
    const {state, actions} = useOvermind()
    const classes = useStyles();

    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
    });

    const onRegister = () => {
        const {email, password} = formik.values
        FayFetch.post(registerEndpoint, null, null, {
            email: email.toString().toLowerCase(),
            password: password.toString().toLowerCase(),
        }, (err, data, ok) => {
            if (ok) {
                actions.setStatus(`${email} successfully registered`)
            } else {
                actions.setStatus(`${email} not registered`)
            }
        })
    }

    const onLogin = () => {
        const {email, password} = formik.values
        FayFetch.post(loginEndpoint, null, null, {
            email: email.toString().toLowerCase(),
            password: password.toString().toLowerCase(),
        }, (err, data, ok) => {
            if (ok) {
                actions.setStatus(`${email} successfully logged in`)
            } else {
                actions.setStatus(`${email} not logged in`)
            }
        })
    }

    return (
        <Grid container direction='row' justify='center' alignContent='center' alignItems='center'>
            <Grid container direction='column' item xs={3} justify='center' alignContent='center'
                  alignItems='center'>
                <form onSubmit={formik.handleSubmit}>
                    <Typography className={classes.topText} variant='h4'>Awesome</Typography>

                    <Typography variant='body2'>Status: {state.status}</Typography>
                    <TextField id="email" type="email" onChange={formik.handleChange} className={classes.commonStyles}
                               fullWidth
                               placeholder="Email"/>
                    <TextField id="password" type="password" onChange={formik.handleChange}
                               className={classes.commonStyles} fullWidth
                               placeholder="Password"/>
                    <Button onClick={() => {
                        onLogin()
                    }} className={classes.commonStyles} fullWidth color='primary'
                            variant='contained'>Login</Button>
                    <Button onClick={() => {
                        onRegister()
                    }} className={classes.commonStyles} fullWidth color='primary'
                            variant='contained'>Register</Button>
                </form>
            </Grid>
        </Grid>
    );
}

export default LoginRegisterDemo;
